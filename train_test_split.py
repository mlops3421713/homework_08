import pandas as pd
from clearml import Dataset, Task

task = Task.init(project_name="Amazon reviews", task_name="preprocessing") 

args = {
    'dataset_project': "Amazon reviews",
    'dataset_name': "RawData",
    'test_size': 0.1,
    'random_state': 177013,
}

task.connect(args)
task.execute_remotely()

dataset_path = Dataset.get(
    dataset_name=args['dataset_name'], 
    dataset_project=args['dataset_project'],
).get_local_copy()

df = pd.read_csv(f"{dataset_path}/data.csv", header=None)

df.columns = ["label", "summary", "text"]

# There are 2 text columns. We assume the vocabulary is the same and merge those.
df["full_text"] = df["summary"].fillna("") + " " + df["text"]
df["full_text"] = df["full_text"].str.strip().str.lower()

# Originally, 2 is positive and 1 is negative (mostly balanced). We transform those to more common 1 and 0.
df["label"] = df["label"] - 1

df_test = df.sample(
    frac=args['test_size'],
    random_state=args['random_state'],
)

df_train = df.copy()[~df.index.isin(df_test.index)]

df_train.to_csv('train_csv', index=False)
df_test.to_csv('test_csv', index=False)

task.upload_artifact(name='df_train', artifact_object='train_csv')
task.upload_artifact(name='df_test', artifact_object='test_csv')
