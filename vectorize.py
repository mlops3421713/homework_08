import pandas as pd

from sklearn.preprocessing import Normalizer
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.pipeline import Pipeline
from joblib import dump
from clearml import Task

task = Task.init(project_name="Amazon reviews", task_name="vectorize") 

args = {
    'preprocessing_task_id': '',
    'vectorizer_method': 'tfidf',
    'scaler_method': 'length',
    'max_features': 100,
    'ngram_range': (1, 1),
}

task.connect(args)
task.execute_remotely()

model_map = {
    "bow": CountVectorizer,
    "tfidf": TfidfVectorizer,
}

# QA: very few methods work with sparse matrices.
scale_map = {
    "length": Normalizer(),
}

vectorizer = Pipeline(
    [
        (
            "vectorize",
            model_map.get(args['vectorizer_method'])(
                stop_words="english",
                ngram_range=args['ngram_range'],
                max_features=args['max_features'],
            ),
        ),
        ("scale", scale_map.get(args['scaler_method'], "length")),
    ]
)

logger = task.get_logger()
logger.report_text(f"Vectorizer: {args['vectorizer_method']}, features: {args['max_features']}, ngram range: {args['ngram_range']}")

preprocess_task = Task.get_task(task_id=args['preprocessing_task_id'])
train_data = pd.read_csv(preprocess_task.artifacts['df_train'].get_local_copy())

vectorizer.fit(train_data["full_text"])

dump(vectorizer, 'vectorizer.joblib')
task.upload_artifact(name='vectorizer', artifact_object='vectorizer.joblib')

