# Amazon reviews: back-to-back metrics study with ClearML

## Preprocessing
Dataset preprocessing steps include:
- merging the summary and the main text into one line (missing summaries count as empty strings);
- casting to lowercase;

Labels were fixed from 1-2 to a conventional 0-1 set. Labels are roughly balanced, no stratification was used.

10% of the data selected for the test set.

## Vectorizing
The following vectorizing methods are supported:
- BoW (`CountVectorizer()`);
- TF-IDF (`TfidfVectorizer()`).

For this particular study, we focus on the TF-IDF apporach with no n-grams, as changing these parameters yields very minor response.

In each case, stop-words were removed (using sklearn built-in 'english' dict).

100 most popular tokens were selected as a compromize between performance and accuracy.

The resulting vectors are further normalized (using sklearn `Normalizer()`) to avoid any problems with gradient descent methods.

## Machine Learning models
This experiment includes:

- robust logistic regression (`SGDClassifier()` using modified Huber loss);
- gradient boosting machine (`HistGradientBoostingClassifier()`) with a fixed tree height of 2.

The number of iterations is limited to 100 in both cases for the reasonable training times.

Basic regularized logistic regression, naive Bayes and LDA are also supported out of the box.

## Evaluation
Model evaluation is done by 5 fold cross-validation during the train stage. Log loss and Brier score are calculated for each fold (box plots available via ClearML artifacts).

Presentation metrics sample is based upon a simple best F1 decision threshold. The recall and precision for the default threshold are supplied for comparison.

ROC and PR curves for the positive class are also available via ClearML artifacts.

## Evaluation results
Results are available to anyone with a clear.ml account:

[SGD pipeline](https://app.clear.ml/projects/1e33b5c982294f8aa7b57ad0f35b4e09/experiments/b1b6766622ba4e1b9e439f1f4659506f/output/execution)

[SGD evaluation stage - easier metrics access](https://app.clear.ml/projects/1e33b5c982294f8aa7b57ad0f35b4e09/experiments/9f73e45c4b0d4812ad6fc5ad65ca1dc5/output/execution)

[GBM pipeline](https://app.clear.ml/projects/cf8e18cce6b34e0e8962977a4c80aef1/experiments/1bddf49a36f14b2c82eabb0b370da4b9/output/execution)

[GBM evaluation stage](https://app.clear.ml/projects/cf8e18cce6b34e0e8962977a4c80aef1/experiments/05c8b5a7829144ca8f81e946b3e718f4/output/execution)

### Proper scoring rules

GBM models display notably smaller log loss while the Brier score and other metrics tend to favor a linear model. This might indicate boosting model being better calibrated. Further calibration studies due.

### Threshold independent positive class metrics

Both ROC_AUC and AP (PR_AUC) favor the linear model very slightly but steadily (about 1-2%), indicating somewhat better performance on the positive class.

### 'Intuitive' metrics

Same as above, both accuracy and the optimal F1 for the positive class favor the linear model, reaching around 0.85 recall and 0.68 precision, yet the GBM model is still just about 1-2% behind.

## Conclusion
Overall, the linear model tends to be very slightly ahead as is, making it more cost effective. The GBM model still has a room for improvement with more/higher trees at the cost of further increase in computational complexity.

Additional studies on calibration recommended.
