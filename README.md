# MLops homework: ClearML

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

This one includes the code and report only - no data, artifacts or CI. Check out `report/report.md` for the clear.ml links and results.

Very similar to the MLflow homework, except for only 2 experiments and using ClearML pipelines instead of snakemake ones.

## Running:

`conda env create -f dev.yaml`

The dataset is expected to be uploaded to clear.ml datasets (see upload.py for the example).

ClearML agent should be configured and running (`conda run -n clearml310 clearml-agent daemon --queue default`).

The easiest way to upload the tasks without extra clearml-task hassle:

`conda activate clearml310`

`python train_test_split.py && python vectorize.py && python train.py && python evaluate.py`

Running an experiment pipeline:

`conda run -n clearml310 python experiment-sgd.py`

`conda run -n clearml310 python experiment-gbm.py`

DISCLAIMER: pipelines were executed via `.run_locally()` since clearml-agent ignores venv and attempts to use system python (WTF), which its code is too obsolete to support.
