import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from joblib import load

from sklearn.metrics import (
    accuracy_score,
    recall_score,
    precision_score,
    f1_score,
    roc_auc_score,
    roc_curve,
    precision_recall_curve,
    average_precision_score,
)

from clearml import Task

task = Task.init(project_name="Amazon reviews", task_name="evaluate") 

args = {
    'preprocessing_task_id': '',
    'vectorizing_task_id': '',
    'training_task_id': '',
}

task.connect(args)
task.execute_remotely()

def calculate_metrics(target_test, probabilities):
    predictions = probabilities > 0.5
    ap = average_precision_score(target_test, probabilities)
    roc_auc = roc_auc_score(target_test, probabilities)

    precision, recall, thresholds = precision_recall_curve(
        target_test,
        probabilities,
    )
    # You may adjust the beta here:
    beta = 1
    f_scores = (
        (1 + beta**2)
        * recall
        * precision
        / (recall + beta**2 * precision + np.finfo(float).eps)
    )
    best_thresh = thresholds[np.argmax(f_scores)]
    best_f = np.max(f_scores)

    optimal_threshold_preds = probabilities > best_thresh

    logger = task.get_logger()
    logger.report_single_value("CV Log loss", cv_logloss.mean())
    logger.report_single_value("CV Brier score", cv_brier.mean())
    logger.report_single_value("ROC_AUC", roc_auc)
    logger.report_single_value("AP", ap)
    logger.report_single_value("F1 at the optimal threshold", best_f)

    logger.report_single_value(
        "Accuracy at the optimal threshold",
        accuracy_score(target_test, optimal_threshold_preds),
    )
    logger.report_single_value(
        "Recall at the optimal threshold",
        recall_score(target_test, optimal_threshold_preds),
    )
    logger.report_single_value(
        "Precision at the optimal threshold",
        precision_score(target_test, optimal_threshold_preds),
    )
    logger.report_single_value(
        "Accuracy at 0.5",
        accuracy_score(target_test, predictions),
    )
    logger.report_single_value(
        "Recall at 0.5", recall_score(target_test, predictions)
    )
    logger.report_single_value(
        "Precision at 0.5",
        precision_score(target_test, predictions),
    )
    logger.report_single_value("F1 at 0.5", f1_score(target_test, predictions))
    logger.report_single_value("Optimal threshold", best_thresh)

    fig, axes = plt.subplots(1, 2, figsize=(10, 4))
    axes[0].plot([0, 1], linestyle="--")
    axes[1].plot([0.5, 0.5], linestyle="--")
    fpr, tpr, _ = roc_curve(target_test, probabilities)
    precision, recall, _ = precision_recall_curve(
        target_test, probabilities
    )
    axes[0].plot(fpr, tpr)
    axes[1].plot(recall, precision)

    axes[0].set(
        xlabel="FPR",
        ylabel="TPR",
        title="ROC curve",
        xlim=(0, 1),
        ylim=(0, 1),
    )
    axes[1].set(
        xlabel="Recall",
        ylabel="Precision",
        title="PR curve",
        xlim=(0, 1),
        ylim=(0, 1),
    )
    logger.report_matplotlib_figure('Curves', '', fig)
    fig, ax = plt.subplots()
    ax.boxplot(cv_logloss, vert=False)
    plt.title("Cross-validation: Log Loss")
    logger.report_matplotlib_figure('Cross-validation: Log Loss', '', fig)
    fig, ax = plt.subplots()
    ax.boxplot(cv_brier, vert=False)
    plt.title("Cross-validation: Brier Score")
    logger.report_matplotlib_figure('Cross-validation: Brier Score', '', fig)


preprocess_task = Task.get_task(task_id=args['preprocessing_task_id'])
val_data = pd.read_csv(preprocess_task.artifacts['df_test'].get_local_copy())

vectorizing_task = Task.get_task(task_id=args['vectorizing_task_id'])
vectorizer = load(vectorizing_task.artifacts['vectorizer'].get_local_copy())

training_task = Task.get_task(task_id=args['training_task_id'])
model = load(training_task.artifacts['model'].get_local_copy())
cv_logloss = load(training_task.artifacts['cv_logloss'].get_local_copy())
cv_brier = load(training_task.artifacts['cv_brier'].get_local_copy())

X_test = vectorizer.transform(val_data["full_text"]).toarray()
y_test = val_data["label"]

probs = model.predict_proba(X_test)[:, 1]

calculate_metrics(y_test, probs)
