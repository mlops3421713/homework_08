from clearml import StorageManager, Dataset

dataset = Dataset.create(
    dataset_project="Amazon reviews", dataset_name="RawData"
)

dataset.add_files(path='raw/data.csv')
#dataset.add_files(path='/home/daiyousei/sources/homework_07/raw/data_tiny.csv')
dataset.upload()
dataset.finalize()
