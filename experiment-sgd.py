from clearml.automation import PipelineController

pipe = PipelineController(
     name='Amazon reviews pipeline - SGD',
     project='Amazon reviews',
     version='0.0.1',
     add_pipeline_tags=False,
)

pipe.set_default_execution_queue('default')

pipe.add_step(
    name="preprocessing",
    base_task_project="Amazon reviews",
    base_task_name="preprocessing",
)

pipe.add_step(
    name="vectorizing",
    base_task_project="Amazon reviews",
    base_task_name="vectorize",
    parameter_override={"General/preprocessing_task_id": "${preprocessing.id}"},
    parents=["preprocessing"],
)

pipe.add_step(
    name="training",
    base_task_project="Amazon reviews",
    base_task_name="train",
    parameter_override={
        "General/preprocessing_task_id": "${preprocessing.id}",
        "General/vectorizing_task_id": "${vectorizing.id}",
    },
    parents=["vectorizing"],
)

pipe.add_step(
    name="evaluation",
    base_task_project="Amazon reviews",
    base_task_name="evaluate",
    parameter_override={
        "General/preprocessing_task_id": "${preprocessing.id}",
        "General/vectorizing_task_id": "${vectorizing.id}",
        "General/training_task_id": "${training.id}",
    },
    parents=["training"],
)

pipe.start_locally()
