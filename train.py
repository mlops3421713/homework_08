import pandas as pd

from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import HistGradientBoostingClassifier

from joblib import dump, load

from clearml import Task

task = Task.init(project_name="Amazon reviews", task_name="train") 

args = {
    'preprocessing_task_id': '',
    'vectorizing_task_id': '',
    'model': 'sgd',
    'random_state': 177013,
    'max_iter': 100,
}

task.connect(args)
task.execute_remotely()

models = {
    "lr": LogisticRegression(
        random_state=args['random_state'],
        max_iter=args['max_iter'],
    ),
    "sgd": SGDClassifier(
        loss="modified_huber",
        random_state=args['random_state'],
        n_jobs=6,
        max_iter=args['max_iter'],
    ),
    "nb": GaussianNB(),
    "lda": LinearDiscriminantAnalysis(),
    "gbm": HistGradientBoostingClassifier(
        random_state=args['random_state'],
        max_iter=args['max_iter'],
        max_depth=2,
    ),
}

model = models.get(
    args['model'],
    LogisticRegression(
        random_state=args['random_state'],
        max_iter=args['max_iter'],
    ),
)

preprocess_task = Task.get_task(task_id=args['preprocessing_task_id'])
train_data = pd.read_csv(preprocess_task.artifacts['df_train'].get_local_copy())

vectorizing_task = Task.get_task(task_id=args['vectorizing_task_id'])
vectorizer = load(vectorizing_task.artifacts['vectorizer'].get_local_copy())


X_train = vectorizer.transform(train_data["full_text"]).toarray()
y_train = train_data["label"]

logger = task.get_logger()
logger.report_text(f"Model: {args['model']}, iterations: {args['max_iter']}")

model.fit(X_train, y_train)

dump(model, 'model.joblib')
task.upload_artifact(name='model', artifact_object='model.joblib')

cv_logloss = -cross_val_score(
    model, X_train, y_train, cv=5, scoring="neg_log_loss"
)
cv_brier = -cross_val_score(
    model, X_train, y_train, cv=5, scoring="neg_brier_score"
)

dump(cv_logloss, 'cv_logloss.joblib')
dump(cv_brier, 'cv_brier.joblib')

task.upload_artifact(name='cv_logloss', artifact_object='cv_logloss.joblib')
task.upload_artifact(name='cv_brier', artifact_object='cv_brier.joblib')
